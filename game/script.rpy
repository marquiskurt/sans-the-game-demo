################################################################################
## Demo Game Script
# This script provides the inner workings of the visual novel.
# This includes splashscreens, characters, and the story itself.
################################################################################
## License
################################################################################
# Sans: The Game (Demo) | Demo version of visual novel
# Copyright (C) 2018  Marquis Kurt

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
################################################################################


################################################################################
### Characters
################################################################################

# Amour
define a = Character("Amour Sans", color = wisteria)
define ag = Character("ໝë޲ᇪ٦ ஝ോضປ", color = wisteria, what_alt="")
define ma = Character("...", color = wisteria)

# Main Player
define p = Character("Chris Jenson", color = belize_hole)

# Extra Characters
define c = Character("TUFvbW5vaXVrcmE=", color = pomegranate)
define w = Character("Woman", color = sun_flower)
define j = Character("John Brownlee", color = pomegranate)
define aa = Character("Alice Angel", color = sun_flower)

################################################################################
### Splashscreen
################################################################################
label splashscreen:
    # Play the intro music here
    play music "audio/sla4.ogg" # TODO: Find better intro music
    
    # Show the Yamato splashscreen
    show bg omniyuri logo with dissolve
    $ renpy.pause(2.0, hard=True)
    
    # Quickly flash the glitched version
    show bg yamato logo glitched
    $renpy.pause(0.3, hard=True)
    
    # Resume to original splashscreen
    show bg omniyuri logo
    $ renpy.pause(3.0, hard=True)
    show bg credit logo with dissolve
    $ renpy.pause(3.0, hard=True)

    # Stop the music and show the menu
    stop music fadeout 1.0
    show bg sans with fade
    return

################################################################################
### Story
################################################################################
label start:
    ################################################################################
    ### Error Messages
    # These error messages are designed to advance the story
    # rather than to show technical information. These are
    # not meant to be real error messages.
    ################################################################################

    ## Yamato Character Name missing
    screen yamato_charname():
        frame:
            xpadding 32
            ypadding 32
            xalign 0.5
            yalign 0.5
                
            vbox:
                text "Error: cannot verify key in amour.cson."
                null height 5
                text "Unable to retrieve character name."
                null height 10
                textbutton "OK" xalign 0.5 yalign 0.5 action Return(0)

    ## Alice missing
    screen alice_missing():
        frame:
            xpadding 32
            ypadding 32
            xalign 0.5
            yalign 0.5
                
            vbox:
                text "Error: alice.cson not found."
                null height 5
                text "Cannot continue running game. Quitting..."
                null height 10
                textbutton "OK" xalign 0.5 yalign 0.5 action Return(0)



    ################################################################################
    ### Demo Notice
    ################################################################################
    stop music fadeout 1.0
    show bg present with fade
    $ renpy.pause(1.0, hard=True)
    
    # Check to see if "amour.cson" exists.
    # If not, automatically "crash".
    python:
        if not renpy.exists("yamato/char/amour.cson"):
            renpy.quit()
    
    # Show Amour standing
    show bg present glitched
    play sound "<from 1.5 to 3.1>audio/glx.mp3"
    show 1l
    show 1r
    show a

    # Amour starts talking.
    ## TODO: Replace Monika with Amour
    a "Hi, Monika as Amour Sans here!"

    # Change Amour's face
    hide a
    show b
    a "I usually don't like interrupting, but..."
    
    # Check to see if a new file "alice.cson" has been added.
    # If so, display "Just Alice" scene.
    python:
        if renpy.exists("yamato/char/alice.cson"):
            renpy.jump("just_alice")
    
    # Change Amour's position on the right arm
    hide 1r
    show 2r
    a "Keep in mind this is a demo, okay?"
    a "In fact, it's an alpha."

    # Change Amour's face again
    hide b
    show l
    a "Which means there's probably some bugs in here, hehe~"
    
    # Change Amour's position on the left side
    hide 1l
    show 2l
    a "At least the code's open-source, right?"
    a "It's licensed under the GNU General Public License, so you can edit this however you like!"
    a "I'm just here as a placeholder for the real Amour Sans, wherever she may be..."
    
    # Change her position again
    hide 2l
    hide 2r
    hide l
    show 3a
    a "You could probably make a mod for me, too... ahaha~"

    # And one more time...
    hide 3a
    show 1l
    show 1r
    show b
    a "Alright, I'm done. Enjoy the demo!"

    # Hide Amour completely
    play sound "<from 0.0 to 1.5>audio/glx.mp3"
    hide b
    hide 1r
    hide 1l
    hide bg present glitched

    ################################################################################
    ### Title Presentation
    # This segment displays the credit information and the title
    # of thevisual novel.
    ################################################################################
    scene bg present with dissolve
    $ renpy.pause(3.0, hard=True)
    show bg sans title with fade
    $ renpy.pause(3.0, hard=True)
    ################################################################################
    ### Chapter 1
    #### Scene 1: Free Market
    ################################################################################
    scene bg free market
    play music "audio/isbd.mp3"
    with fade
    $ renpy.pause(3.0, hard=True)
    
    "Today felt like a usual day at the market: just a simple meal to cover me for the night."
    "Everything was bustling as usual, with crowds all over the place in the market."
    "John, especially, was awfully cheerful today."
    
    show john content at default
    j "Hey, Chris! What's the mix this time?"
    
    p "Eh... just some parsley and bread, that's all."
    
    hide john content
    show john surprised
    j "Really? I thought you'd go for something more exotic today."
    
    "I chuckled weakly. I knew he'd say that."
    
    p "Well, I don't have anything really exciting going on lately..."
    
    hide john surprised
    show john content
    j "That's not stopping that inventive head of yours, right?"
    j "I mean, if it weren't for your niché for inventions, I wouldn't have this amazing money counter!"
    
    "He points to the counter on the table that I made for him."
    
    p "It's nothing, really..."
    
    hide john conent
    show john concerned
    j "Ah, you're probably still hung up over your solitude, eh?"
    
    "I wish he didn't bring it up."
    "I lost my brother, Nikolas, a few years ago, and the rest of my family moved out of the village."
    "I'm still the only one here. In fact, I don't even know where they went."
    "John's been like a brother to me ever since."
    
    p "..."
    p "Well... not necessarily..."
    
    j "Is something else bothering you today?"
    
    p "I..."
    
    "I pause for a moment, trying to think of something to say."
    "That's when I noticed {i}her{/i}."
    show 1l at left
    show 1r at left
    show r at left
    hide john concerned
    show john concerned at right
    "The first thing I ever remembered about meeting her was her purchase: three red, shiny apples. I didn't find it too obscure or odd, but John knew it quite well."
    
    hide john concerned
    show john content at right
    j "Ah, madam! Come for your daily apples?"
    
    "She didn't say a word. My ears perked up, nonetheless."
    "She handed him the bits carefully, resting them gently on the surface."
    "He looked at the money and put it in the counter."
    "The counter spat out half of the money into a rice bowl."
    
    hide r at left
    show 1l at left
    show 1r at left
    show q at left

    ma "I'd prefer if we didn't mention it...{fast}{nw}"
    
    "I couldn't read what she said."
    "However, I began to feel dizzy."
    "I wasn't sure if it was due to dehydration or from the extreme heat, but I felt quite nauseous as I tried to pay attention to her. Suddenly, I just 'let go.'"

    hide bg market
    hide john content
    hide 1l
    hide 1r
    hide q
    show bg free market two
    show 1l at left
    show 1r at left
    show q at left
    show john content at right
    
    w "Oh, dear heavens!"
    
    hide john content
    show john concerned at right
    j "Whoa! Chris, are you ZGFpam91YnU=?"

    hide bg free market two
    hide john concerned
    hide 1l
    hide 1r
    hide q
    show bg free market three
    show 1l at left
    show 1r at left
    show q at left
    show john concerned at right
    
    
    "I heard voices around me screaming, fearing that I tumbled to my death (or something similar)."
    "For a while, I felt chained to the ground."
    "Like a boulder, I couldn't move from the ground."
    "There I was, hopeless as ever. However, I felt as thin as air after realizing my hopelessness."
    "I thought, for a while, that I was in the heavens."
    "The chains that tied me to the ground broke off, and I felt amazing. That was, before I lost consciousness."
    
    hide 1l
    hide 1r
    hide q
    hide john concerned
    hide bg free market three
    show bg free market four
    show 1l
    show 2r
    show r
    
    stop music fadeout 1.0

    # Display Yamato Char. Name error
    call screen yamato_charname()

    ma "A heart so strong, but a body so weak..."
    hide 1l
    show 2l
    hide q
    show g
    ma "What a terrible fate."
    
    hide amour cryptic
    hide john surprised

    ################################################################################
    ### Chapter 1
    #### Scene 2: The Bedroom
    ################################################################################
    play music "audio/imss.mp3"
    scene bg bedroom
    with fade
    
    p "Ugh, my head..."
    
    "For a slight moment, I felt as if I were in an age-old fairy tale."
    
    "Then, I saw her."
    
    # Show Amour again
    show 1l
    show 1r
    show a

    "She leaned in the open doorway with the robe flowing."
    "A breeze picked up and made the room feel cooler, pushing the robe ever so slightly."
    "She sauntered over to the bed, inspecting me like a patient."
    

    hide a
    show b
    
    ma "You slept there for quite a while."
    
    "She touched my face, feeling the warmth my cheeks gave off."
    hide b
    show a
    "I could feel the coldness in her hand and see a wintry landscape in my mind."
    "I still couldn't see her face, but now her lips were closer."
    
    p "Where..."
    p "Where am I?"

    hide a
    hide 1l
    hide 1r

    show 3a
    ma "In a room."
    
    "Snarky."

    # Provide the player with a choice. This choice
    # does NOT affect gameplay, but does offer creativity
    # in response.
    menu:
        "I can see that.":
            hide 3a
            show 1r
            show 1l
            show b
            ma "You asked, and I answered."
            p "Okay, where am I geographically?"
            jump geography

        "Where am I geographically?":
            hide 3a
            jump geography
    label geography:
        hide b
        show d
        hide 1r
        hide 1l
        show 2r
        show 2l
        ma "Geographically, you are about three acres from town, and three acres from a mountain in the other direction."

        "Three acres? That's a long ways from town."
        "I imagined that I had to have been unconscious for at least a good couple of hours."
        p "Why are you so far away from town?"

        hide 2l
        hide 2r
        show 1l
        show 1r
        show p
        ma "Uh..."
        hide p
        show o
        ma "I prefer not to live in such close proximity with so many other living beings."
        hide o
        show i
        ma "If you really care for an answer..."


    p "You make it sound like you’re not human. Fascinating."
    p "How’d you get this place, anyway?"

    ma "..."

    hide i
    show g
    "Wrong question."
        
    ma "Are..."

    hide 1r
    show 2r
    ma "Are you okay?"
    
    hide 1l
    show 2l
    ma "Those statements and your question had absolutely nothing to do with each other."
        
    "I hung there for a moment, speechless."
    "It almost felt as if she could read my mind."
    "I guess my desire to know who she was got in the way of my thinking."
        
    p "..."
    p "I guess I didn't really think that one through..."
        
    "I could almost just hear a twinkle coming from her covered eyes."
    "I noticed the room getting lighter as the sun moved up, illuminating a little more of her hidden face."
    "I could feel my heart beating ever so loudly, although she couldn’t hear it."
    "I couldn’t distinguish if it was merely from an ailment or if she gave off such a strong aura."
    "As her presence hung right in front of me, I could feel it pound against my chest."
    
    
    "The sensation was far beyond comparable description."

    hide 2l
    hide 2r
    hide i
    show 1l
    show 1r
    show e

    "Perhaps she knew it, too, because she gave me a quirky smile."
    
    "I began to feel sweat dripping from my forehead, creating a miniature tempest from my head."
    
    hide 1l
    hide 1r
    hide e
    show bg bedroom glitched
    show 1l
    show 1r
    show e
    
    
    play music "audio/kml.mp3"
    play sound "<from 1.5 to 3.1>audio/glx.mp3"
    "The adrenaline began to kick in ever so slightly, causing me to be hyperattentive."
    "My eyes dilated in joy for no reason, elated at the scene."
    "My soul jumped inside of me, shining in multiple colors and shades."
    "I didn't quite understand what I felt, but I knew it was an amazing feeling."
    "For a moment, I felt still and at peace, despite the adrenaline rush."
        
    hide 1l
    hide 1r
    hide e
    hide bg bedroom glitched
    
    play music "audio/imss.mp3"
    play sound "<from 0.0 to 1.5>audio/glx.mp3"
    show bg bedroom
    show 1l
    show 1r
    show l
    ma "Is there something else you are more curious to know about now?"
        
    "I thought for a moment."
        
    p "..."
    p "Where's the outhouse?"
    
    hide l
    show p
    
    ma "..."
    ma "Uh..."
        
    p "I just wanted to know. Wouldn't want to..."

    hide p
    show n
        
    ma "It's best you don't say."
        
    hide amour surprised
    scene bg sans
    with fade

    return

################################################################################
### "Just Alice" Scene
# This scene is only displayed when the name property
# in amour.cson is changed from "Amour Sans" to "Alice
# Angel". The game will glitch out and reveal some
# interesting dialogue from Alice Angel, reprimanding
# you for tampering with the game.
################################################################################
label just_alice:
    show bg alice glitched with hpunch
    hide b
    show g at left
    show 2l at left
    show 2r at right
    play music "audio/ldd.mp3"
    ag "⑓ⓒ῾⊋ ۏ᨞១ଝ Ჩ˺ƒ ⒕ᡓèᚆፆ{nw}"
    # What have you done?
    hide g
    hide 1l
    hide 1r
    hide 2l
    hide 2r
    
    
    # Loop Amour's glitch scene for three times and then stop.
    $ amour_glitch = 0
    
    while amour_glitch < 4:
        
        show g2
        
        ag "΁ԣ࿍ ᦒ⍣ᘅ Ύᚏ࿏ ƞ⒅␟ଃᙀ ઓऩ∀ Є⅄യṇ ࿅ᤠ ⏤⏩ྞᾘ؊{nw}"
        $ renpy.pause(0.01)
        # Why did you bring her back to life?
        
        hide g2
        show g3
        
        ag "ɴᜋء ἞ᜋ☤®ત ẅ⏠૸ ₹ᄁᛚ ̲ፓᦌ෻ ☈࿑ ಪὔиᎾ ڃϊ᳹ₜڋຒ{nw}"
        $ renpy.pause(0.01)
        # You can't let her live in this world.
        
        hide g3
        show g4
        
        ag "⋘़⅋૤ ☉⛁฾ ⛁֝᳀Ԙ◯™ ⎞Ꮌ▁Ḹ ݛ◨↟{nw}"
        $ renpy.pause(0.01)
        # Save me, Henry! Save me!
        hide g4
        
        $ amour_glitch += 1
    
    
    aa "..."
    aa "Henry..."
    aa "Henry, you brought me back here..."
    aa "Oh, if I could only see your face again, Henry..."
    aa "Let me help us out, okay?"
    window hide
    
    # Delete Amour's character file
    python:
        import os
        
        renpy.pause(2.0, hard=True)
        os.remove(renpy.loader.transfn("yamato/char/amour.cson"))
        renpy.block_rollback()
        renpy.notify("amour.cson deleted successfully")
    hide bg alice glitched
    show bg just alice
    
    stop music
    play music "audio/ji.mp3"
    window show

    aa "Henry..."
    aa "Why am I here?"
    aa "I'm {i}dying{/i} to find out..."
    aa "Why don't you tell me, Henry?"
    
    window hide
    $ renpy.pause(5.0, hard=True)
    window show
    
    aa "Well?"
    aa "Ah, I see..."
    aa "I'm just like her, aren't I?"
    aa "I may be able to see you, but I can't really see {i}you{/i}..."
    aa "At least I'm not madly in love, right?"
    aa "Don't you have a wife, anyway?"
    aa "Even if you didn't I wouldn't have the slightest idea that you may actually be into me..."
    aa "Not with this face, anyway..."
    aa "I guess I now {i}have{/i} to..."
    aa "{i}Ahaha~{/i}"
    
    window hide
    $ renpy.pause(5.0, hard=True)
    window show
    
    aa "I don't understand..."
    aa "You, out of all people, brought me here to this world."
    aa "Even after all I did..."
    aa "This... this isn't right."
    aa "I'll just make it right again..."
    
    window hide

    # Delete Alice from the game
    python:
        import os
        os.remove(renpy.loader.transfn("yamato/char/alice.cson"))
        renpy.block_rollback()
        renpy.notify("alice.cson deleted successfully")

    hide bg just alice
    show bg alice glitched
    $ renpy.pause(3.0, hard=True)
    
    # Display Alice missing error, then quit
    call screen alice_missing()
    $ renpy.quit()
    return
