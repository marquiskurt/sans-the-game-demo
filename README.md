#  Sans: The Game (Demo)
_Open-source demo of the game **Sans: The Game**_

## About this project
**Sans: The Game (Demo)** is the open-source demo version of the game _Sans: The Game_. This demo offers a glimpse into what an ideal Ren'Py game could look like. This software is licensed under the GNU GPL v3 license.

### Features
* Custom GUI for menus, dialog boxes, and story dialogs
* Character images
* Background images
* Custom Colors module for character names
* Music and sounds for story
* Custom fonts for GUI and story
* Secret scene when `alice.cson` is added to Yamato characters folder